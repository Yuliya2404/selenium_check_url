import pytest
from webdriver_manager.firefox import GeckoDriverManager
from selenium.webdriver import Firefox


@pytest.fixture
def ebay():
    ff_browser = Firefox(executable_path=GeckoDriverManager().install())
    ff_browser.maximize_window()
    ff_browser.get("https://www.ebay.com/")
    yield ff_browser
    ff_browser.quit()
