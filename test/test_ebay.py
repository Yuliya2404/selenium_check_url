from selenium.webdriver import Firefox

class TestEbay:
    def test_ebay(self, ebay: Firefox):
        target_url = "https://www.ebay.com/"
        ebay_title = "Electronics, Cars, Fashion, Collectibles & More | eBay"
        assert ebay.current_url == target_url, f"Incorrect url {ebay.current_url}, should be {target_url}"
        assert ebay.title == ebay_title, f"Incorrect title {ebay.current_url}, should be {target_url}"

