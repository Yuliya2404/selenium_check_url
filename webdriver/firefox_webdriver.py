from time import sleep

from selenium.webdriver import Firefox, DesiredCapabilities
from webdriver_manager.firefox import GeckoDriverManager

capabilities = DesiredCapabilities.FIREFOX
ff_browser = Firefox(executable_path=GeckoDriverManager().install(), desired_capabilities=capabilities)
ff_browser.maximize_window()
ff_browser.get("https://www.ebay.com")
sleep(5)
print(ff_browser.title)
ff_browser.quit()